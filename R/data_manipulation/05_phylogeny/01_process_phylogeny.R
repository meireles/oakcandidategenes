library("oakphylo")

########################################
# Paths
########################################

input_path_pathspecies_id = "data/processed/master_sample_lookup.rds"
output_path_phy           = "data/processed/phylogeny/phy.rds"

########################################
# Data
########################################

phy   = oak_phylo
sp_id = readRDS(input_path_pathspecies_id)


########################################
# Cleanup
########################################

sp_names = unique(sp_id$species_name)

## Add the genus name to sp_names
sp_names = paste("quercus", sp_names, sep = "_")

## Clean up tip names in phylogeny
phy$tip.label = gsub("quercus_brandegei", "quercus_brandegeei", phy$tip.label)

phy = oakphylo::phy_trim_names(phy)
phy = oakphylo::phy_exclude_truly_redundant_tips(phy)

## Filter phy to match data
phy = oakphylo::phy_keep_tips(phy, sp_names)

########################################
# Write out
########################################

saveRDS(phy, output_path_phy)

