library("raster")
library("rgdal")
library("sp")

#######################################
# Paths
#######################################

input_path_occurences_all   = "data/processed/gis/occurences_samples_and_gbif.rds"
input_path_occurences_sampl = "data/processed/gis/samples_occurence.rds"

input_path_bioclim_north_am = "data/processed/gis/bioclim_layers_north_am.grd"

output_path_bioclim_all     = "data/processed/gis/bioclim_data_samples_and_gbif.rds"
output_path_bioclim_sampl   = "data/processed/gis/bioclim_data_samples.rds"


#######################################
# Data
#######################################

## Bioclim layers
bioclim_layers = raster::stack(input_path_bioclim_north_am)

## Occurence
occ_all   = readRDS(input_path_occurences_all)
occ_sampl = readRDS(input_path_occurences_sampl)

#######################################
# Extract data from rasters
#######################################

bioclim_occ_all = lapply(occ_all, function(y){
    raster::extract(x = bioclim_layers, y = y)
})

bioclim_occ_sampl = lapply(occ_sampl, function(y){
    raster::extract(x = bioclim_layers, y = y)
})

#######################################
# Cleanup
#######################################

## Remove NAs if any
bioclim_occ_all = lapply(bioclim_occ_all, function(x){
    x[complete.cases(x), ]
    })


bioclim_occ_sampl = lapply(bioclim_occ_sampl, function(x){
    x[complete.cases(x), ]
})

#######################################
# Write out
#######################################

saveRDS(bioclim_occ_all, output_path_bioclim_all)
saveRDS(bioclim_occ_sampl, output_path_bioclim_sampl)
