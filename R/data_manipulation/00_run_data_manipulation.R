source("R/data_manipulation/01_process_sample_id.R")

source("R/data_manipulation/02_occurence/00_run_occurence.R")
source("R/data_manipulation/03_bioclim/00_run_bioclim.R")
source("R/data_manipulation/04_sequences/00_run_sequences.R")
source("R/data_manipulation/05_phylogeny/00_run_phylogeny.R")

rm(list = ls(all = TRUE))
