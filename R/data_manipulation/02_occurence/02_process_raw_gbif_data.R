library("sp")
library("maptools")
library("rgdal")
library("raster")
library("geosphere")

#######################################
# Paths
#######################################

input_path_raw_occurence  = "data/raw/gbif/raw_virentes_gbif.rds"
input_path_continents_shp = "data/raw/continents/continent.shp"

output_path_occurence     = "data/processed/gis/gbif_occurence.rds"
output_path_north_am_map  = "data/processed/gis/north_am_map.rds"

output_path_gbif_distr_map = "temporary/gbif_distribution_maps.pdf"


#######################################
# Read Data
#######################################

raw_occurences = readRDS(input_path_raw_occurence)
continents     = maptools::readShapeSpatial(input_path_continents_shp)
longlat        = c("decimalLongitude", "decimalLatitude")

#######################################
# Cleanup & Transform
#######################################

#######################################
## General Cleanup

e = raster::extent(-130, -45, 5, 50)
north_america = continents[continents$CONTINENT == "North America", ]
north_america = raster::crop(north_america, e)


occurences     = lapply(raw_occurences, function(x){
    sp::SpatialPointsDataFrame(coords = x[ , longlat], data = x)
})


occurences     = lapply(occurences, function(x){
    sp::remove.duplicates(x)
})


occurences     = lapply(occurences, function(x){
    x[which( ! is.na( sp::over(x, north_america))) , ]
})

#######################################
## Detailed cleanup

## quercus_sagreana is the cuban live oak, but it is under quercus_oleoides and quercus_virginiana in gbif.
## Transfering those cuban oaks to q_sagreana

q_sagraeana_ol = occurences[["quercus_oleoides"]][["country"]] == "Cuba"
q_sagraeana_vi = occurences[["quercus_virginiana"]][["country"]] == "Cuba"

occurences[["quercus_sagraeana"]] = sp::merge(occurences[["quercus_oleoides"]][ q_sagraeana_ol, ],
                                             occurences[["quercus_virginiana"]][ q_sagraeana_vi, ])

occurences[["quercus_oleoides"]] = occurences[["quercus_oleoides"]][ ! q_sagraeana_ol, ]
occurences[["quercus_virginiana"]] = occurences[["quercus_virginiana"]][ ! q_sagraeana_vi, ]



## IMPORTANT !!!
## The project is using the epithet as the species name, that is,
## "quercus_minima" will become "minima"
names(occurences) = gsub("quercus_", "", names(occurences))


##############################################################################
# Function find outliers
#######################################

find_outlier = function(x, threshold = 3, return_index = TRUE){
    x     = as.vector(x)
    q     = quantile(x)
    low   = q[["25%"]]
    upp   = q[["75%"]]
    iqr   = upp - low
    t_low = low - (iqr * threshold)
    t_upp = (iqr * threshold) + upp

    out   = x >= t_upp | x <= t_low

    if(return_index){
        out = which(out)
    }

    return(out)
}

##############################################################################

outliers = lapply(occurences, function(x){
    mu  = colMeans(x@coords)
    dis = geosphere::distm(mu, x)
    out = find_outlier(dis, threshold = 3, return_index = FALSE)
    out
})

occurences = mapply(function(x, o){ x[ ! o , ] }, occurences, outliers )


#######################################
# Write out
#######################################

saveRDS(occurences, output_path_occurence)
saveRDS(north_america, output_path_north_am_map)


pdf(output_path_gbif_distr_map)

for(i in seq_along(occurences)){
    plot(north_america, main = names(occurences[i]))
    points(occurences[[i]], col = "red")

}

dev.off()
