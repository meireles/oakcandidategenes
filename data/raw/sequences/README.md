# Reference sequence retrieval

This is the log file detailing the retrieval of refernce sequences from genbank.

* The unaligned genes reside inside `data/raw/by_gene_unaligned`

* Each gene was manually blasted with blastn (megablast if possible) AND restricting taxa to: "Arabdopsis", "Prunus" and "Fagales"

* One or two annotated result hits were retrieved as reference sequences.

* Results from blast were saved in dir `blast_alignment_result`

* Reference loci were manually parsed and a summary, including sequence and begining and end of CDS, were encoded in the file `ref_seqs_from_blast.csv`.

## ref\_seqs\_from\_blast.csv

Headers in this file are pretty self-explanantory. `cds_start` and `cds_end` may have multiple entries, sepatared by semicolon `;`. They were encoded such that the nth entry of `cds_start` matches the nth entry of `cds_end`.


## Processing data with genbank reference sequences

`by_gene_unaligned_plus_blasted`
