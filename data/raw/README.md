# Raw Files Readme


## 1. data\_virentes.xlsx

Excel file with sequence data. Compiled by JRS on 2016-03-22.

The 7 columns in the file are:

1. **0:** Index. Unknown origin.
		
2. **Fasta char:**
 Default indicator of the start of a fasta seq.
		
3. **Lab_ID:** Individual sample ID. Serves as a key for both the DNA samples / data @ the JRS lab as well as for JCB's lab physiology measurements. 

4. **Species:** Species name abbreviations

5. **Gene:** Gene identifier, divided at the underscore char such that: `{gene_name}_{complement}`. The complement format distinguishes exons from introns:

	* introns: Only numeric (e.g. `ERS1_2`), where the number indicates which intron it is.
	* exons:   Prefixed `EX{n}`, where n indicates which exon it is.
			
6. **Allele:** Indication of allele. JRS & DB were able to call heterozygotes most of the time.
		
7. **Sequence:** DNA sequences strings. Roughly aligned.


## 2. gene\_description\_table.xlsx


## 3. physiology\_data.xlsx

From file `Physiology-Data.xlsx`


## 4. master\_sample\_id.xlsx

From file `Virentes_Master_ID_11_17_14.xlsx` and