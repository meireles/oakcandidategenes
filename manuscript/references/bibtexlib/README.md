# bibtexlib
--

**bibtexlib** is my centralized bibliographic library and meant for my personal use. 

This project is designed to be used as a *subrepo* (using [`git subrepo`](https://github.com/ingydotnet/git-subrepo)) in manusripts version controlled with git. I use this approach in my [paper_template](https://bitbucket.org/meireles/paper_template) repo.

As of 2016 June 4th, I am using the [issue/142 branch](https://github.com/ingydotnet/git-subrepo/tree/issue/142) of [git subrepo](https://github.com/ingydotnet/git-subrepo) instead of master.

Changes to this repo come almost exclusivelly from Mendley syncing my references to the `library.bib` bibtex file.

