Summary of parameters and input files.
Please check that all is correct while calculation is starting...

There are 1 loci.

There are 1 populations.

Burn in: 50000
Thining interval: 10
Sample size: 5000
Resulting total number of iterations: 100000
Nb of pilot runs: 20
Length of each pilot run: 5000

Allele counts:
Pop. 1 locus 1 :   1   1   2   1   2   1 

